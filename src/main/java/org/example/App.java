package org.example;



import org.example.tp3.exo1.Student;
import org.example.tp3.exo1.Teacher;
import org.example.tp3.exo2.Appartment;
import org.example.tp3.exo2.Person;

public class App
{
    public static void main( String[] args )
    {

        // TP 3

      //  Person p = new Person();
     //   p.sayHello();

        Student s = new Student();
        s.goToClasses();
        s.setAge(15);
        s.sayHello();
        s.displayAge();

        Teacher t = new Teacher();
        t.setAge(40);
        t.sayHello();
        t.explain();


        Appartment MyApartament = new Appartment();
        Person person = new Person();
        person.name = "Thomas";
        person.house = MyApartament;
        person.display();



    }
}
