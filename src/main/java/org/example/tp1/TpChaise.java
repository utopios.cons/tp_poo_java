package org.example.tp1;

public class TpChaise {


    private int nbrePieds;
    private String couleur;
    private String materiaux;


    public TpChaise(int nbrePieds, String couleur, String materiaux) {
        this.nbrePieds = nbrePieds;
        this.couleur = couleur;
        this.materiaux = materiaux;
    }


    public TpChaise(){

    }



    public void display(){
        System.out.println(" ----- Affiche d'un nouvel objet -----");
        System.out.printf("Je suis une Chaise, avec %d pieds en %s et de couleur %s .",this.nbrePieds, this.materiaux, this.couleur);
    }

    public int getNbrePieds() {
        return nbrePieds;
    }

    public void setNbrePieds(int nbrePieds) {
        this.nbrePieds = nbrePieds;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getMateriaux() {
        return materiaux;
    }

    public void setMateriaux(String materiaux) {
        this.materiaux = materiaux;
    }





    @Override
    public String toString() {
        return "TpChaise{" +
                "nbrePieds=" + nbrePieds +
                ", couleur='" + couleur + '\'' +
                ", materiaux='" + materiaux + '\'' +
                '}';
    }
}
