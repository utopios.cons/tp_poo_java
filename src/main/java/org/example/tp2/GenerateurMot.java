package org.example.tp2;

import java.util.Random;

public class GenerateurMot implements GenerateurInterface{


    private String[] mots = new String[] { "google", "amazon", "facebook", "apple", "microsoft" };
    private Random random = new Random();


    @Override
    public String generer() {
        return mots[random.nextInt(0,mots.length)];
    }
}
