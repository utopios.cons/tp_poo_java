package org.example.tp2;

public class LePendu {
    private int nbEssai;
    private String masque;
    private String motATrouve;

    private GenerateurMot generateurMot;

    public LePendu(int nbEssai, String masque, String motATrouve) {
        this.nbEssai = nbEssai;
        this.masque = masque;
        this.motATrouve = motATrouve;
    }

    public LePendu(){
        this.nbEssai = 10;
    }

    public boolean TestChar(char c)
    {
        //A coder
        boolean found = false;
        String masqueTmp = "";
        for(int i=0; i < motATrouve.length(); i++)
        {
            if(this.motATrouve.charAt(i) == c)
            {
                found = true;
                masqueTmp += c;
            }
            else
            {
                masqueTmp += masque.charAt(i);
            }
        }
        masque = masqueTmp;
        if(!found)
        {
            this.nbEssai--;
        }
        return found;
    }

    public boolean TestWin()
    {
        return motATrouve == masque;
    }

    public void GenererMasque()
    {
        generateurMot= new GenerateurMot();

        String masqueTmp = "";
        motATrouve = generateurMot.generer();
        for(int i=0; i < motATrouve.length(); i++)
        {
            masqueTmp += "*";
        }
        masque = masqueTmp;
    }

}
